/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myClinic;

/**
 *
 * @author joel.helling904
 */
public class Visit {
    
    private double visitPay;
    
    public Visit()
    {
        this.visitPay = 0.0;
    }
    
    public double getVisitPay()
    {
        return this.visitPay;
    }
    
    public void setVisitPay(int numberOfHours,double rate)
    {
        this.visitPay = numberOfHours*rate; 
    }
}
