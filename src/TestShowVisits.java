import java.util.*;
import myClinic.Visit;
import myGUI.ShowVisit;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joel.helling904
 */
public class TestShowVisits {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Random rng = new Random();
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter some number of visits: ");
        int numberOfVisits = scan.nextInt();
        System.out.println("Please enter the rate of a doctors visit: ");
        double rateOfVisit = scan.nextDouble();
        
        Visit[] visits = new Visit[numberOfVisits];
        ShowVisit showVisit = new ShowVisit();
        
        for(int i = 0; i < visits.length; i++)
        {
            visits[i] = new Visit();
            visits[i].setVisitPay(rng.nextInt(10) + 1,rateOfVisit );
        }
       
        for(int i = 0; i < visits.length; i++)
            showVisit.display(visits[i]);
        
    }
}
